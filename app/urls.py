from django.urls import path, include
from .views import (
    GetAllProducts,
    AddProduct,
    ProductsRUD,

    GetAllSuppliers,
    AddSupplier,
    SuppliersRUD,

    GetAllCustomers,
    AddCustomer,
    CustomersRUD,

    BuyProduct,

    GetAllOrders,
    ClearOrdersData,
    GetAllOrderDetails,
    ClearOrderDetailsData
)
# from .Views import (
#     products_views,
#     customers_views,
#     suppliers_views
# )

urlpatterns = [
    # products views
    path('products/get-all', GetAllProducts.as_view()),
    path('product/add', AddProduct.as_view()),
    path('product/<str:product_id>', ProductsRUD.as_view()),

    # suppliers views
    path('suppliers/get-all', GetAllSuppliers.as_view()),
    path('supplier/add', AddSupplier.as_view()),
    path('supplier/<str:supplier_id>', SuppliersRUD.as_view()),

    # customers views
    path('customers/get-all', GetAllCustomers.as_view()),
    path('customer/add', AddCustomer.as_view()),
    path('customer/<str:customer_id>', CustomersRUD.as_view()),

    # orders views
    path('orders/get-all', GetAllOrders.as_view()),
    path('orders/delete-all', ClearOrdersData.as_view()),

    # order details views
    path('order-details/get-all', GetAllOrderDetails.as_view()),
    path('order-details/delete-all', ClearOrderDetailsData.as_view()),

    # actions
    path('actions/buy', BuyProduct.as_view()),

    # orders views
    # path('api/add_student', AddStudentAPIView.as_view()),
    # path('api/<str:student_id>', StudentAPIsView.as_view()),
]